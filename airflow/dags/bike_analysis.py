from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import os, os.path
from io import StringIO
from datetime import datetime
import boto3
import pandas as pd

AIRFLOW_HOME = os.getenv('AIRFLOW_HOME')
AWS_ACCES_KEY = 'AKIAULJGTRNNYKXSSNLB'
AWS_SECRET_KEY = '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'
DOWNLOAD_BUCKET = 'hetic-big-data-project'
UPLOAD_BUCKET =  'hetic-big-data-project'


def download_file(bucket, key):
    s3 = boto3.resource(service_name="s3",aws_access_key_id=AWS_ACCES_KEY, aws_secret_access_key=AWS_SECRET_KEY)
    b = s3.Bucket(bucket)
    files = b.objects.filter(Prefix=key)
        
  
    csv_files = [f for f in files if f.key.endswith('.csv')]
    for f in csv_files :
        s3.meta.client.download_file(bucket, f.key, AIRFLOW_HOME + f'/dags/data/S3_files/{f.key}')

def count_stations(df):
    return df['start station name'].value_counts()

def get_average_duration(df):
    return df.groupby(['birth year', 'gender'])['tripduration'].mean()


def analyse_data():
    for file in os.listdir(AIRFLOW_HOME + '/dags/data/S3_files/bicycle'):
        if file.endswith('.csv'):
            df_citibike = pd.read_csv(AIRFLOW_HOME + f'/dags/data/S3_files/bicycle/{file}', sep=',', error_bad_lines=False)

            df_count_stations = count_stations(df_citibike)
            df_average_duration = get_average_duration(df_citibike)

            df_count_stations.to_csv(AIRFLOW_HOME + f'/dags/data/S3_files/analysis_results/bicycle/{file}_count_stations.csv')
            df_average_duration.to_csv(AIRFLOW_HOME + f'/dags/data/S3_files/analysis_results/bicycle/{file}_average_duration.csv')

    

def upload_to_s3(files, bucket, key_folder):
    s3 = boto3.resource(service_name="s3",aws_access_key_id='AKIAULJGTRNNYKXSSNLB', aws_secret_access_key='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')

    for filename in files:
        source = AIRFLOW_HOME + '/dags/data/S3_files/analysis_results/bicycle/' + filename
        key = key_folder + filename
        s3.Bucket(bucket).upload_file(source, key)


        


dag = DAG(
    'bike_analysis',
    description='DAG analyse bike csv',
    schedule_interval='*/2 * * * *',
    start_date=datetime(2021, 5, 16),
    catchup=False
)

download_from_s3 = PythonOperator(
        task_id='download_from_s3',
        python_callable=download_file,
        op_kwargs={'bucket': DOWNLOAD_BUCKET, 'key': 'bicycle' },
        dag=dag)

analyse_data = PythonOperator(
        task_id='analyse_data',
        python_callable=analyse_data,
        dag=dag)

upload_to_s3 = PythonOperator(
        task_id='upload_to_s3',
        python_callable=upload_to_s3,
        op_kwargs={'files': os.listdir(AIRFLOW_HOME + '/dags/data/S3_files/analysis_results/bicycle'), 'bucket': UPLOAD_BUCKET, 'key_folder': 'reporting/bicycle/' },
        dag=dag)


download_from_s3 >> analyse_data >> upload_to_s3