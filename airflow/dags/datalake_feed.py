from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator
import os, os.path
import fnmatch
from datetime import datetime
import boto3

AIRFLOW_HOME = os.getenv('AIRFLOW_HOME')


'''
Créez un DAG  se nommant datalake_feed.py permettant d’analyser une liste de CSV dans un répertoire local nommé data et qui affichera à l’aide d’un PythonOperator :
le nombre de CSV total 
le nombre de CSV contenant dans leurs titre le mot “citibike” 
le nombre de CSV contenant dans leurs titre le mot “trip”
'''

def start():
    return 'Starting'

def analyse_csv(directory):

    # Total number of csv files
    csv_files = fnmatch.filter(os.listdir(directory), '*.csv')
    nb_files = (len(csv_files))

    # Number of csv files containing 'citibike'
    citibike_files = fnmatch.filter(os.listdir(directory), '*citibike*')
    nb_citibike_files = (len(citibike_files))

    # Number of csv files containing 'trip'
    trip_files = fnmatch.filter(os.listdir(directory), '*trip*')
    nb_trip_files = (len(trip_files))

    return trip_files, citibike_files, nb_files, nb_citibike_files, nb_trip_files

'''
Tous les CSV contenant le mot “trip” seront envoyé dans votre bucket dans le répertoire “taxi”
Tous les CSV contenant le mot “citibike” seront envoyé dans votre bucket dans le répertoire “bicycle” 
'''

UPLOAD_BUCKET =  'hetic-big-data-project'
trip_files = analyse_csv(AIRFLOW_HOME + '/dags/data')[0]
citibike_files = analyse_csv(AIRFLOW_HOME + '/dags/data')[1]

def upload_to_s3(files, bucket, key_folder):
    s3 = boto3.resource(service_name="s3",aws_access_key_id='AKIAULJGTRNNYKXSSNLB', aws_secret_access_key='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')

    for filename in files:
        source = AIRFLOW_HOME + '/dags/data/' + filename
        key = key_folder + filename
        s3.Bucket(bucket).upload_file(source, key)


dag = DAG(
    'dag_analyse_csv',
    description='DAG analyse csv',
    schedule_interval='*/5 * * * *',
    start_date=datetime(2021, 5, 7),
    catchup=False
)

start = PythonOperator(
        task_id='start',
        python_callable=start,
        dag = dag
    )

analyse_csv = PythonOperator(
        task_id='analyse_csv',
        python_callable=analyse_csv,
        op_kwargs={'directory': AIRFLOW_HOME + '/dags/data'},
        dag = dag
    )

upload_trip_files_to_s3 = PythonOperator(
        task_id='upload_tripfiles_to_s3',
        python_callable=upload_to_s3,
        op_kwargs={'bucket': UPLOAD_BUCKET, 'files':trip_files, 'key_folder': 'taxi/'},
        dag = dag
    )

upload_citibike_files_to_s3 = PythonOperator(
        task_id='upload_citibike_to_s3',
        python_callable=upload_to_s3,
        op_kwargs={'bucket': UPLOAD_BUCKET, 'files':citibike_files, 'key_folder': 'bicycle/' },
        dag = dag
    )

start >> analyse_csv >> upload_trip_files_to_s3 >> upload_citibike_files_to_s3
