from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from datetime import datetime
import os, os.path
import boto3
import geopandas as gpd
import pandas as pd


AIRFLOW_HOME = os.getenv('AIRFLOW_HOME')
AWS_ACCES_KEY = 'AKIAULJGTRNNYKXSSNLB'
AWS_SECRET_KEY = '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'
UPLOAD_BUCKET =  'hetic-big-data-project'



def analyse_data():
    
    count = 0
    
    for bicycle_file in os.listdir(AIRFLOW_HOME + '/dags/data/S3_files/bicycle'):
        if bicycle_file.endswith('.csv'):
            df_citibike = pd.read_csv(AIRFLOW_HOME + f'/dags/data/S3_files/bicycle/{bicycle_file}', sep=',', error_bad_lines=False)

            df_bike_location = df_citibike[['start station latitude', 'start station longitude']]
            df_bike_location.columns = ['lat', 'lon']

    
            for taxi_file in os.listdir(AIRFLOW_HOME + '/dags/data/S3_files/taxi'):
                if taxi_file.startswith('green'):
                    df_tripdata = pd.read_csv(AIRFLOW_HOME + f'/dags/data/S3_files/taxi/{taxi_file}', sep=',', error_bad_lines=False)
                    df_location = df_tripdata[['PULocationID', 'DOLocationID']]

                    df_taxi_zone = pd.read_csv(AIRFLOW_HOME + f'/dags/data/taxi+_zone_lookup.csv', sep=',', error_bad_lines=False)
                    dict_ = df_taxi_zone['Borough'].to_dict()

                    df_location.PULocationID = df_location.PULocationID.astype(int)
                    df_location = df_location.replace({"PULocationID": dict_})

                    # Get latitude and longitude from geopandas dataset
                    boros = gpd.read_file(gpd.datasets.get_path("nybb"))

                    boro_locations = gpd.tools.geocode(boros.BoroName)

                    boro_locations['lon'] = boro_locations.geometry.x
                    boro_locations['lat'] = boro_locations.geometry.y

                    # Formatting boroughs
                    boro_locations['address'] = boro_locations['address'].apply(lambda x: x.split(',')[0])
                    boro_locations['address'] = boro_locations['address'].apply(lambda x: x.split(' ')[0] if (x == 'Bronx County' or x == 'Queens County')  else x)

                    df_ny = pd.merge(boro_locations,df_location, how='right', left_on='address',right_on='PULocationID')
                    df_ny.dropna(inplace=True)

                    df_ny = df_ny[['lat', 'lon']]

                    df_geo = pd.concat([df_bike_location, df_ny])
            
            df_geo.to_csv(AIRFLOW_HOME + f'/dags/data/S3_files/analysis_results/reporting_global/map_{count}.csv')
            count += 1


def upload_to_s3(files, bucket, key_folder):
    s3 = boto3.resource(service_name="s3",aws_access_key_id='AKIAULJGTRNNYKXSSNLB', aws_secret_access_key='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')

    for filename in files:
        source = AIRFLOW_HOME + '/dags/data/S3_files/analysis_results/reporting_global/' + filename
        key = key_folder + filename
        s3.Bucket(bucket).upload_file(source, key)


dag = DAG(
    'compare_analysis',
    schedule_interval='*/5 * * * *',
    start_date=datetime(2021, 5, 16),
    catchup=False
)

analyse_data = PythonOperator(
        task_id='analyse_data',
        python_callable=analyse_data,
        dag=dag)

upload_to_s3 = PythonOperator(
        task_id='upload_to_s3',
        python_callable=upload_to_s3,
        op_kwargs={'files': os.listdir(AIRFLOW_HOME + '/dags/data/S3_files/analysis_results/reporting_global'), 'bucket': UPLOAD_BUCKET, 'key_folder': 'reporting/global/'},
        dag=dag)


analyse_data >> upload_to_s3