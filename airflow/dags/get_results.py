from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import os, os.path
from datetime import datetime
import boto3
import pandas as pd
import psycopg2
from sqlalchemy import create_engine


AIRFLOW_HOME = os.getenv('AIRFLOW_HOME')
AWS_ACCES_KEY = 'AKIAULJGTRNNYKXSSNLB'
AWS_SECRET_KEY = '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'
DOWNLOAD_BUCKET = 'hetic-big-data-project'


def download_file(bucket, key):
    s3 = boto3.resource(service_name="s3",aws_access_key_id=AWS_ACCES_KEY, aws_secret_access_key=AWS_SECRET_KEY)
    b = s3.Bucket(bucket)
    files = b.objects.filter(Prefix=key)
        
  
    csv_files = [f for f in files if f.key.endswith('.csv')]
    for f in csv_files :
        #filename = f.key.split('/')[2]
        s3.meta.client.download_file(bucket, f.key, AIRFLOW_HOME + f'/dags/data/S3_files/{f.key}')


def create_df():
    df_list = []
    for file in os.listdir(AIRFLOW_HOME + '/dags/data/S3_files/reporting/global'):
        df = pd.read_csv(AIRFLOW_HOME + f'/dags/data/S3_files/reporting/global/{file}', sep=',', index_col=False )
        df_list.append(df)
    return df_list

def create_db():
    """ create tables in the PostgreSQL database"""
    commands = (
        """
        CREATE DATABASE ny_transports
        """,
        """
        CREATE TABLE IF NOT EXISTS geo  (
            latitude FLOAT,
            longitude FLOAT
        )
        """
      )
    try:
        # read the connection parameters
        param_dic = {
            "user"      : "airflow",
            "password"  : "airflow"
        }
        # connect to the PostgreSQL server
        conn = psycopg2.connect(param_dic)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

    df_list = create_df()

    engine = create_engine('postresql://airflow:airflow@localhost:5432/ny_transports')
    
    for df in df_list:
        df.to_sql('geo', engine)



dag = DAG(
    'get_results',
    description='DAG create PostgreSQL database',
    schedule_interval='*/5 * * * *',
    start_date=datetime(2021, 5, 16),
    catchup=False
)

download_from_s3 = PythonOperator(
        task_id='download_from_s3',
        python_callable=download_file,
        op_kwargs={'bucket': DOWNLOAD_BUCKET, 'key': 'reporting/global' },
        dag=dag)

create_df = PythonOperator(
        task_id='create_df',
        python_callable=create_df,
        dag=dag)

create_db = PythonOperator(
        task_id='create_db',
        python_callable=create_db,
        dag=dag)



download_from_s3 >> create_df >> create_db